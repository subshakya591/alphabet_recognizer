import cv2


def preprocess(gray):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
    dilated = cv2.morphologyEx(gray, cv2.MORPH_DILATE, kernel)
    diff1 = 255 - cv2.subtract(dilated, gray)
    median = cv2.medianBlur(dilated, 15)
    diff2 = 255 - cv2.subtract(median, gray)
    normed = cv2.normalize(diff2, None, 0, 255, cv2.NORM_MINMAX)
    cv2.waitKey(1)
    return normed

